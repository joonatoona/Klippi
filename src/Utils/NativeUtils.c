#include "NativeUtils.h"

#ifdef __unix__
#include <unistd.h>
#include <pwd.h>
#elif _WIN32
#include <windows.h>
#endif

const char* getUsername() {
#if defined(__unix__) || defined(__APPLE__)
    struct passwd *pws;
    pws = getpwuid(geteuid());
    return pws->pw_name;
#elif defined(_WIN32)
    char username[UNLEN+1];
    DWORD username_len = UNLEN+1;
    GetUserName(username, &username_len);
    return username;
#endif
}
